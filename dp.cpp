// recursion 
int dp(int nums){
    if(nums <= 1){
        return nums;
    }
    return dp(nums-1) + dp(nums-2);
}

// Top down
int dp(int nums){
    vector<int> store(nums,0);
    return dpHelper(nums, store);
}

int dpHelper(int nums , vector<int> store){
    if(nums <= 1) return nums;
    store[nums] = dpHelper(nums-1,store) + dpHelper(nums-2,store);
    return store[nums];
}

// bottom up(forward version iteration + memorization)

int dp(int nums){
    if(nums <= 1) return nums;
    vector<int> dp(nums,0);
    dp[0] = 0;
    dp[1] = 1;
    for(int i = 2 ; i < nums; i++){
        dp[i] = dp[i-1] + dp[i-2];
    }
    return dp[nums];
}

// bottom up (backword version )
int dp(int nums){
    if(nums <= 1) return nums;
    vector<int> dp(nums+2,0);
    dp[0] = 0;
    dp[1] = 1;
    for(int i = 1; i< nums; i++){
        dp[i+1] += dp[i];
        dp[i+2] += dp[i];
    }
    dp[nums];
}