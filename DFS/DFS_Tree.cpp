Node<int>* dfs(Node<int>* root, int target) {
    if (root == nullptr) return nullptr;
    if (root->val == target) return root;
    // return non-null return value from the recursive calls
    Node<int>* left = dfs(root->left, target);
    if (left != nullptr) return left;
    // at this point, we know left is null, and right could be null or non-null
    // we return right child's recursive call result directly because
    // - if it's non-null we should return it
    // - if it's null, then both left and right are null, we want to return null
    return dfs(root->right, target);
}