void dfs(Node<int>* root, std::unordered_set<Node<int>*> visited) {
    for (Node<int>* neighbor : node->neighbors) {
        if (visited.count(neighbor)) {
            continue;
        }
        // insert and return pair type
        visited.emplace(neighbor);
        dfs(neighbor, visited);
    }
}
