##### Source : https://spicyboyd.blogspot.com/2018/07/umlclass-diagram-relationships.html
##### Only for self study usage
# Relationships 關係

## Instance-level 物件層級
* Dependency 依賴
* Association 關聯
* Aggregation 聚合
* Composition 組合

## Dependency 依賴
* 定義
    * 相依
    * A references B (as a method parameter or return type)
    * 通常用在方法參數、回傳值
* 箭頭指向 被使用者

## Example 例子
1. "客戶"使用"供應商"的參數
    # ![](D1.jpg)
2. "課程表"使用"課程"的參數
    # ![](D2.jpg)
3. "訂單"使用"付款系統"的參數
    # ![](D3.jpg)

## Association 關聯
* 定義
    * 結合、關聯，class之間有關係
    * A has-a C object (as a member variable)
    * 通常用在屬性、全域變數
* 箭頭指向 被擁有者

## 表示法
1. 直線 + 結合方向
    # ![](A1.jpg)
2. 箭頭 (常用)
    # ![](A2.jpg)

## 數量
# ![](/A3.jpg)

## Example 例子
1. "人"有"住址"的變數
    # ![](A4.jpg)
2. "車子"有"買車的人"的變數
    # ![](A5.jpg)
3. "訂單"使用"付款系統"的參數
    # ![](D3.jpg)

## 比較：Dependency, Association

| | Dependency  | Association |
| -------------| ------------- |:-------------:|
| 概念| A uses a B     | A has a C     |
| |   A 使用 B    |  A 有 C   |
|箭頭 | 虛線      | 實線     |
| | ![](DArrow.jpg)      | ![](AArow.jpg)     |
|例子 | "課程表"使用"課程"的參數      | "人"有"住址"的變數     |

## Association 關聯
* 定義
    * 聚合，弱 整體與部分的關係
    * 整體可以脫離部分而單獨存在
    * 整體與部分具有各自的生命周期
* 菱形指向 整體

## Example 例子
1. "車子"擁有這些零件：“引擎”、“車門”、“輪子”
    # ![](AG1.jpg)
2. "學校"擁有"學生"們
    # ![](AG2.jpg)

## Composition 組合
* 定義
    * 組合，強 整體與部分的關係
    * 整體不可脫離部分而存在
    * 整體的生命周期結束也就意味著部分的生命周期結束
* 菱形指向 整體

## Example 例子
1. "手指"是"手"的一部份
    # ![](C1.jpg)
2. "員工"們是"公司"的一部份
    # ![](C2.jpg)

## 比較：Aggregation, Composition

| | Aggregation  | Composition |
| -------------| ------------- |:-------------:|
| 概念| A owns a B     | C is a part of A     |
| |   A 擁有 B    |  C 是 A 的一部份   |
|箭頭 | 虛線      | 實線     |
| | ![](AgArrow.jpg)      | ![](CArow.jpg)     |
|例子 | "學校"擁有"學生"們      | "員工"們是"公司"的一部份     |
| | 學生畢業離開學校，學生、學校還是單獨存在      | 員工離開公司，員工存在，但公司可能就不存在了     |

## ![](C3.jpg)

## Realization / Implementation 實現 / 實作
* 定義
    * 實作interface介面
* 箭頭指向 interface

## Example 例子
1. “現金”、“信用卡"都要實作"付款系統”
    # ![](R1.jpg)
2. JSP
    # ![](R2.jpg)
    
## Generalization / Inheritance 泛化 / 繼承
* 定義
    * 繼承關係
* 箭頭指向 父類別

## Example 例子
1. “學生”、“教授"都是"人”
    # ![](G1.jpg)
2. Java class
    # ![](G2.jpg)    
    
## 比較：Realization, Generalization
| | Realization  | Generalization |
| -------------| ------------- |:-------------:|
| 概念| B implements A     | C extend A     |
| |   B 實作 A    |  C 繼承自 A   |
|箭頭 | 虛線      | 實線     |
| | ![](RArrow.jpg)      | ![](GAroow.jpg)     |
|例子 | “現金”、“信用卡"都要實作"付款系統”	      | “學生”、“教授"都是"人”     |

## 總整理、比較
Dependency < Association < Aggregation < Composition < Realization/Implementation = Generalization/Inheritance

## ![](image.jpg)

## 比較：Realization, Generalization

| | 概念  |  | |符號|
| -------------| ------------- |-------------|-------------|-------------|
| Dependency| A uses a B     | 使用     |  參數、回傳 |![](RArrow.jpg)
| Association|   A has a B    |  有   |全域變數  |![](AArrow.jpg)
|Aggregation | A owns a B      | 擁有     | 整體、部分 |![](AgArrow.jpg)
| Composition| B is a part of A      | 一部分     | 整體、部分 |![](CArrow.jpg)
|Realization | B implements A      | 實作     |介面  |![](DArrow.jpg)
|Generalization | B extends A	      | 繼承     | 	父子類別 |![](GAroow.jpg)

# Reference 參考資料
* Relationships：https://creately.com/blog/diagrams/class-diagram-relationships/
* wiki 英文版：https://en.wikipedia.org/wiki/Class_diagram
* wiki 中文版：https://zh.wikipedia.org/wiki/類別圖
* Uml class Diagram：
https://www.slideshare.net/ShubhamShah001/uml-class-diagram-46300557
UML超新手入門（3）類別圖型 - 結合關係：http://www.codedata.com.tw/java/umltutorial-03/
* UML类之间的六大关系总结：
https://crane-yuan.github.io/2016/08/02/The-relationship-of-uml-class/
* 第9章 類別圖與物件圖：
http://web.ydu.edu.tw/~alan9956/doc100/100-02.sa/ooad-uml-chap09.pdf
