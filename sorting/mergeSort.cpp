#include <iostream>
#include <vector>

using namespace std;

void mergeSort(vector<int>& nums, int l, int r){
    if(l < r){
        int m = (l+r)/2;
        mergeSort(nums, l , m);
        mergeSort(nums,m+1, r);
        merge(nums,l,m,r);
    }
}
void merge(vector<int>& nums, int l , int m , int r){
    vector<int> arr1;
    vector<int> arr2;
    int a1Size = m - l + 1 , a2Size = r-m;
    for(int i = 0 ; i < a1Size; i++){
        arr1.push_back(nums[i+l]);
    }
    for(int i = 0 ; i < a2Size; i++){
        arr2.push_back(nums[i+m+1]);
    }

    int i = 0 , j = 0 , k = l;
    while(i < a1Size && j < a2Size){
        if(arr1[i] <= arr2[j]){
            nums[k] = arr1[i++];
        }else{
            nums[k] = arr2[j++];
        }
        k++;
    }
    while(i < a1Size){
        nums[k++] = arr1[i++];
    }

    while(j < a2Size){
        nums[k++] = arr2[j++];
    }
}
