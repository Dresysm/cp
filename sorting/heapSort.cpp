#include<iostream>
#include<vector>

using namespace std;

void heapSort(vector<int>& nums){
    // build max heap , start from last tree root
    for(int i = nums.size()/2 -1 ; i >= 0; i--){
        heapify(nums,i);
    }
    // sorting 
    for(int i = nums.size() -1 ; i >= 0; i--){
        swap(nums[i],nums[0]);
        heapify(nums,0);
    }


}
void heapify(vector<int>& nums, int i){
    int size = nums.size();
    int larger = i;
    int left = 2 * i +1;
    int right = 2 * i +2;
    
    if(left < size && nums[left] > nums[larger]){
        larger = left;
    }
    if(right < size && nums[right] > nums[larger]){
        larger = right;
    }

    if(larger != i){
        swap(nums[larger], nums[i]);
        heapify(nums,larger);
    }


}