#include <iostream>
#include <vector>
using namespace std;


class Solution {
public:
    void xxxxxxx(vector<int>& nums) {
        quickSort(nums, 0, nums.size()-1);
    }

    void quickSort(vector<int>& nums, int l ,int r){
        if(l < r){
            int p = parition(nums,l,r);
            quickSort(nums,l,p-1);
            quickSort(nums,p+1,r);
        }
    }


    int parition(vector<int>& nums, int l ,int r){
        int maxIdx = l , p = nums[r];
        for(int i = l ; i < r; i++){
            if(nums[i] <= p ){
                swap(nums[i],nums[maxIdx]);
                maxIdx++;
            }
        }
        swap(nums[maxIdx],nums[r]);
        return maxIdx;
    }
};

