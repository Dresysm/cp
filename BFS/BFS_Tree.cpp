Node<T>* bfs(Node<T>* root) {
    std::queue<Node<T>*> q;
    q.push(root);
    while (q.size() > 0) {
        // copy the top of queue into node
        Node<T>* node = queue.front();
        // pop the top of the queue
        q.pop();
        // find the node child
        for (Node<T>* child : node->children) {
            if (is_goal(child)) {
                return FOUND(child);
            }
            q.push(child);
        }
    }
    return NOT_FOUND;
}
