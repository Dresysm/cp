###### source: 
* https://muatik.medium.com/oop-solid-with-examples-d3dc310d72c3
* https://www.educative.io/answers/what-are-the-solid-principles-in-java


## Single Responsibility Principle
A class should have only one reason to change
### before

```
class Product {
  constructor(title, price, taxRate) {
    this.title = title;
    this.price = price;
    this.taxRate = taxRate;
  }
  calculateTax() {
    return this.price * this.taxRate;
  }
}
const table = new Product('a nice table', 55, 0.1);
console.log(table.calculateTax(table));
// output: 5.5
```

### After
```
class Product {
  constructor(title, price, taxRate) {
    this.title = title;
    this.price = price;
    this.taxRate = taxRate;
  }
getPrice() {
    return this.price;
  }
  
  getTaxRate() {
    return this.taxRate;
  }
}
class TaxCalculator {
  static calculateTax(product) {
    return product.getPrice() * product.getTaxRate();
  }
}
const table = new Product('a nice table', 55, 0.1);
console.log(TaxCalculator.calculateTax(table))
```

## Open-closed principle
Software entities (e.g., classes, modules, functions) should be open for an extension, but closed for modification.
### before
```
public class VehicleCalculations {
    public double calculateValue(Vehicle v) {
        if (v instanceof Car) {
            return v.getValue() * 0.8;
        if (v instanceof Bike) {
            return v.getValue() * 0.5;

    }
}
```

### after
```
public class Vehicle {
    public double calculateValue() {...}
}
public class Car extends Vehicle {
    public double calculateValue() {
        return this.getValue() * 0.8;
}
public class Truck extends Vehicle{
    public double calculateValue() {
        return this.getValue() * 0.9;
}
```

## Liskov substitution principle
The Liskov Substitution Principle (LSP) applies to inheritance hierarchies such that derived classes must be completely substitutable for their base classes.

### not apply LSP
The Square class has extra constraints,i.e., the height and width must be the same. Therefore, substituting Rectangle with Square class may result in unexpected behavior.
```
public class Rectangle {
    private double height;
    private double width;
    public void setHeight(double h) { height = h; }
    public void setWidht(double w) { width = w; }
    ...
}
public class Square extends Rectangle {
    public void setHeight(double h) {
        super.setHeight(h);
        super.setWidth(h);
    }
    public void setWidth(double w) {
        super.setHeight(w);
        super.setWidth(w);
    }
}
```



### apply LSP
```
class Container {
  store(thing) {
    this.thing = thing;
  }
}
class Basket extends Container {
  store(thing) {
    if (thing.type === "fluid") {
      throw new Error("cannot store fluid")
    }
    this.thing = thing;
  }
}
const put = (container, thing) => container.store(thing)
const basicContainer = new Container();
const basketContainer = new Basket();
const thing = {"type": "fluid"}
put(basicContainer, thing)
put(basketContainer, thing)
```


## Interface segregation principle
The Interface Segregation Principle (ISP) states that clients should not be forced to depend upon interface members they do not use. In other words, do not force any client to implement an interface that is irrelevant to them.

### before
```
interface Clock {
    void setAlarm(Instant instant);
    float readThermometer();
    void tuneInToRandomRadio();
}
class AncientClock implements Clock {
    @Override
    public void setAlarm(Instant instant) {
        // set the alarm 
    }
    @Override
    public float readThermometer() {
        throw new UnsupportedOperationException("ancient clock does not have thermometer");
    }
    @Override
    public void tuneInToRandomRadio() {
        // tune in to one of the stations
    }
}
class ModernClock implements Clock {
    @Override
    public void setAlarm(Instant instant) {
        // set the alarm 
    }
    @Override
    public float readThermometer() {
        return 5; // return some value
    }
    @Override
    public void tuneInToRandomRadio() {
        throw new UnsupportedOperationException("modern clock cannot play radio");
    }
}
class DigitalOven {
    public void startCooking(Clock clock) {
        //...
        clock.setAlarm(Instant.now());
        //...
    }
}
```

### after 
```
interface Thermometer {
    float readThermometer();
}
interface RadioPlayer {
    void tuneInToRandomRadio();
}
interface Alarm {
    void setAlarm(Instant instant);
}
class AncientClock implements Alarm, RadioPlayer {
    @Override
    public void setAlarm(Instant instant) {
        // set the alarm
    }
    @Override
    public void tuneInToRandomRadio() {
        // tune in to one of the stations
    }
}
class ModernClock implements Alarm, Thermometer {
    @Override
    public void setAlarm(Instant instant) {
        // set the alarm
    }
    @Override
    public float readThermometer() {
        return 5; // return some value
    }
}
class DigitalOven {
    public void startCooking(Alarm alarm) {
        //...
        alarm.setAlarm(Instant.now());
        //...
    }
}
```

## Dependency inversion principle
The Dependency Inversion Principle (DIP) states that we should depend on abstractions (interfaces and abstract classes) instead of concrete implementations (classes). The abstractions should not depend on details; instead, the details should depend on abstractions.
### before
```
public class Car {
    private Engine engine;
    public Car(Engine e) {
        engine = e;
    }
    public void start() {
        engine.start();
    }
}
public class Engine {
   public void start() {...}
}
```

### after
```
public interface Engine {
    public void start();
}

public class Car {
    private Engine engine;
    public Car(Engine e) {
        engine = e;
    }
    public void start() {
        engine.start();
    }
}
public class PetrolEngine implements Engine {
   public void start() {...}
}
public class DieselEngine implements Engine {
   public void start() {...}
}
```

