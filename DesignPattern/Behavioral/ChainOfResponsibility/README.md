# Chain of Responsibility Pattern

1. Create an abstract logger class.

```
public abstract class AbstractLogger {
   public static int INFO = 1;
   public static int DEBUG = 2;
   public static int ERROR = 3;

   protected int level;

   //next element in chain or responsibility
   protected AbstractLogger nextLogger;

   public void setNextLogger(AbstractLogger nextLogger){
      this.nextLogger = nextLogger;
   }

   public void logMessage(int level, String message){
      if(this.level <= level){
         write(message);
      }
      if(nextLogger !=null){
         nextLogger.logMessage(level, message);
      }
   }

   abstract protected void write(String message);
	
}

```

2. Create concrete classes extending the logger.

```
public class ConsoleLogger extends AbstractLogger {

   public ConsoleLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("Standard Console::Logger: " + message);
   }
}

public class ErrorLogger extends AbstractLogger {

   public ErrorLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("Error Console::Logger: " + message);
   }
}

public class FileLogger extends AbstractLogger {

   public FileLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("File::Logger: " + message);
   }
}
```

3. Create different types of loggers. Assign them error levels and set next logger in each logger. Next logger in each logger represents the part of the chain.
```
public class ChainPatternDemo {
	
   private static AbstractLogger getChainOfLoggers(){

      AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
      AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
      AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

      errorLogger.setNextLogger(fileLogger);
      fileLogger.setNextLogger(consoleLogger);

      return errorLogger;	
   }

   public static void main(String[] args) {
      AbstractLogger loggerChain = getChainOfLoggers();

      loggerChain.logMessage(AbstractLogger.INFO, 
         "This is an information.");

      loggerChain.logMessage(AbstractLogger.DEBUG, 
         "This is an debug level information.");

      loggerChain.logMessage(AbstractLogger.ERROR, 
         "This is an error information.");
   }
}
```



result

```
Standard Console::Logger: This is an information.
File::Logger: This is an debug level information.
Standard Console::Logger: This is an debug level information.
Error Console::Logger: This is an error information.
File::Logger: This is an error information.
Standard Console::Logger: This is an error information.
```

## result explaination

For

```
      loggerChain.logMessage(AbstractLogger.INFO,"This is an information.");
```

In AbstractLogger, **this.level = 3** , because getChainOfLoggers() return **errorLogger**,and the **level** that pass to logMessage is 1.

For logMessage(), 
1. this.level(3) <= level(1), not match and nextLogger is not null, so do recursion
2. this.level(2) <= level(1), not match and nextLogger is not null, so do recursion
3. this.level(1) <= level(1), match , write the mesage and nextLogger null, so stop recursion

============================================================

For

```
      loggerChain.logMessage(AbstractLogger.ERROR, "This is an error information.");
```

In AbstractLogger, **this.level = 3** , because getChainOfLoggers() return **errorLogger**,and the **level** that pass to logMessage is 2.

For logMessage(), 
1. this.level(3) <= level(2), not match and nextLogger is not null, so do recursion
2. this.level(2) <= level(2), match , write the mesage and nextLogger is not null, so do recursion
3. this.level(1) <= level(2), match , write the mesage and nextLogger null, so stop recursion
 
============================================================

For

```
      loggerChain.logMessage(AbstractLogger.DEBUG,"This is an debug level information.");
```

In AbstractLogger, **this.level = 3** , because getChainOfLoggers() return **errorLogger**,and the **level** that pass to logMessage is 3.

For logMessage(), 
1. this.level(3) <= level(3), match , write the mesage and nextLogger is not null, so do recursion
2. this.level(2) <= level(3), match , write the mesage and nextLogger is not null, so do recursion
3. this.level(1) <= level(3), match , write the mesage and nextLogger null, so stop recursion

