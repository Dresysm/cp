void backTracking(vector<vector<int>> result , vector<int> subset , vector<int> nums, int start){
    if(nums.size() == start){
        result.push_back(subset);
        return;
    }
    for(int i = start; i < nums.size(); i++){
        subset.push_back(nums[i]);
        dfs(result,subset,nums,i+1);
        subset.pop_back();
    }
}